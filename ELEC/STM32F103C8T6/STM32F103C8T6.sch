EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 71 78
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_ST_STM32F1:STM32F103C8Tx U7101
U 1 1 5FF57301
P 5500 4000
F 0 "U7101" H 5950 2500 50  0000 C CNN
F 1 "STM32F103C8Tx" H 6200 2400 50  0000 C CNN
F 2 "Package_QFP:LQFP-48_7x7mm_P0.5mm" H 4900 2600 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/CD00161566.pdf" H 5500 4000 50  0001 C CNN
	1    5500 4000
	1    0    0    -1  
$EndComp
Text HLabel 1500 7300 0    50   Input ~ 0
2.0÷3.6V
Text Label 1950 7300 0    50   ~ 0
2.0÷3.6V
Wire Wire Line
	1500 7300 1950 7300
Text Label 5200 2350 2    50   ~ 0
2.0÷3.6V
Wire Wire Line
	5200 2350 5300 2350
Wire Wire Line
	5400 2500 5400 2350
Wire Wire Line
	5400 2350 5300 2350
Connection ~ 5300 2350
Wire Wire Line
	5300 2350 5300 2500
Wire Wire Line
	5500 2500 5500 2350
Wire Wire Line
	5500 2350 5400 2350
Connection ~ 5400 2350
Wire Wire Line
	5600 2500 5600 2350
Wire Wire Line
	5600 2350 5500 2350
Connection ~ 5500 2350
Text HLabel 1500 7400 0    50   Input ~ 0
GND
Text Label 1950 7400 0    50   ~ 0
GND
Wire Wire Line
	1500 7400 1950 7400
Wire Wire Line
	5700 2350 5700 2500
Connection ~ 6150 2350
Wire Wire Line
	6150 2350 5700 2350
Connection ~ 6500 2750
Wire Wire Line
	6150 2750 6500 2750
Wire Wire Line
	6150 2650 6150 2750
Connection ~ 6500 2350
Wire Wire Line
	6150 2350 6150 2450
Wire Wire Line
	6500 2350 6150 2350
$Comp
L Device:C_Small C7109
U 1 1 5FF70814
P 6150 2550
F 0 "C7109" H 6058 2504 50  0000 R CNN
F 1 "10n" H 6058 2595 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6150 2550 50  0001 C CNN
F 3 "~" H 6150 2550 50  0001 C CNN
	1    6150 2550
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C7111
U 1 1 5FF6B8E8
P 6500 2550
F 0 "C7111" H 6592 2596 50  0000 L CNN
F 1 "100n" H 6592 2505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6500 2550 50  0001 C CNN
F 3 "~" H 6500 2550 50  0001 C CNN
	1    6500 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 2650 6500 2750
Wire Wire Line
	6500 2350 6500 2450
Wire Wire Line
	6500 2750 7000 2750
Wire Wire Line
	6650 2350 6500 2350
Text Label 7000 2750 0    50   ~ 0
GND
Wire Wire Line
	6850 2350 7050 2350
Text Label 7050 2350 0    50   ~ 0
2.0÷3.6V
Text Label 5000 5650 2    50   ~ 0
GND
Wire Wire Line
	5600 5500 5600 5650
Wire Wire Line
	5600 5650 5500 5650
Wire Wire Line
	5500 5500 5500 5650
Connection ~ 5500 5650
Wire Wire Line
	5500 5650 5400 5650
Wire Wire Line
	5400 5500 5400 5650
Connection ~ 5400 5650
Wire Wire Line
	5400 5650 5300 5650
Wire Wire Line
	5300 5500 5300 5650
Connection ~ 5300 5650
Wire Wire Line
	5300 5650 5000 5650
$Comp
L Device:C_Small C7105
U 1 1 5FF7E953
P 4950 1400
F 0 "C7105" H 5042 1446 50  0000 L CNN
F 1 "100n" H 5042 1355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4950 1400 50  0001 C CNN
F 3 "~" H 4950 1400 50  0001 C CNN
	1    4950 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 1200 4950 1300
$Comp
L Device:C_Small C7106
U 1 1 5FF7EF10
P 5300 1400
F 0 "C7106" H 5392 1446 50  0000 L CNN
F 1 "100n" H 5392 1355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5300 1400 50  0001 C CNN
F 3 "~" H 5300 1400 50  0001 C CNN
	1    5300 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 1200 5300 1300
$Comp
L Device:C_Small C7107
U 1 1 5FF7F69C
P 5650 1400
F 0 "C7107" H 5742 1446 50  0000 L CNN
F 1 "100n" H 5742 1355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5650 1400 50  0001 C CNN
F 3 "~" H 5650 1400 50  0001 C CNN
	1    5650 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 1200 5650 1300
$Comp
L Device:C_Small C7108
U 1 1 5FF7FDEA
P 6000 1400
F 0 "C7108" H 6092 1446 50  0000 L CNN
F 1 "100n" H 6092 1355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6000 1400 50  0001 C CNN
F 3 "~" H 6000 1400 50  0001 C CNN
	1    6000 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 1200 6000 1300
$Comp
L Device:C_Small C7110
U 1 1 5FF80527
P 6350 1400
F 0 "C7110" H 6442 1446 50  0000 L CNN
F 1 "100n" H 6442 1355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6350 1400 50  0001 C CNN
F 3 "~" H 6350 1400 50  0001 C CNN
	1    6350 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 1200 6350 1300
$Comp
L Device:C_Small C7112
U 1 1 5FF80CDD
P 6700 1400
F 0 "C7112" H 6792 1446 50  0000 L CNN
F 1 "100n" H 6792 1355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6700 1400 50  0001 C CNN
F 3 "~" H 6700 1400 50  0001 C CNN
	1    6700 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 1200 6700 1300
$Comp
L Device:C_Small C7104
U 1 1 5FF81563
P 4600 1400
F 0 "C7104" H 4692 1446 50  0000 L CNN
F 1 "10u" H 4692 1355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4600 1400 50  0001 C CNN
F 3 "~" H 4600 1400 50  0001 C CNN
	1    4600 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 1200 4600 1300
$Comp
L Device:C_Small C7113
U 1 1 5FF88C95
P 7050 1400
F 0 "C7113" H 7142 1446 50  0000 L CNN
F 1 "100n" H 7142 1355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7050 1400 50  0001 C CNN
F 3 "~" H 7050 1400 50  0001 C CNN
	1    7050 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 1200 7050 1300
Text Label 4200 1200 2    50   ~ 0
2.0÷3.6V
Wire Wire Line
	4200 1200 4600 1200
Wire Wire Line
	4950 1200 4600 1200
Connection ~ 4600 1200
Wire Wire Line
	4950 1200 5300 1200
Connection ~ 4950 1200
Wire Wire Line
	5300 1200 5650 1200
Connection ~ 5300 1200
Wire Wire Line
	5650 1200 6000 1200
Connection ~ 5650 1200
Wire Wire Line
	6000 1200 6350 1200
Connection ~ 6000 1200
Wire Wire Line
	6350 1200 6700 1200
Connection ~ 6350 1200
Wire Wire Line
	6700 1200 7050 1200
Connection ~ 6700 1200
Wire Wire Line
	7050 1500 7050 1650
Wire Wire Line
	7050 1650 6700 1650
Wire Wire Line
	4600 1500 4600 1650
Connection ~ 4600 1650
Wire Wire Line
	4950 1500 4950 1650
Connection ~ 4950 1650
Wire Wire Line
	4950 1650 4600 1650
Wire Wire Line
	5300 1500 5300 1650
Connection ~ 5300 1650
Wire Wire Line
	5300 1650 4950 1650
Wire Wire Line
	5650 1500 5650 1650
Connection ~ 5650 1650
Wire Wire Line
	5650 1650 5300 1650
Wire Wire Line
	6000 1500 6000 1650
Connection ~ 6000 1650
Wire Wire Line
	6000 1650 5650 1650
Wire Wire Line
	6350 1500 6350 1650
Connection ~ 6350 1650
Wire Wire Line
	6350 1650 6000 1650
Wire Wire Line
	6700 1500 6700 1650
Connection ~ 6700 1650
Wire Wire Line
	6700 1650 6350 1650
Text Label 4200 1650 2    50   ~ 0
GND
Wire Wire Line
	4200 1650 4600 1650
Text Label 4350 4000 2    50   ~ 0
PB_2_BOOT_1
Wire Wire Line
	4800 4000 4350 4000
Text Label 4350 2900 2    50   ~ 0
BOOT_0
Wire Wire Line
	4800 2900 4350 2900
Text Label 4350 2700 2    50   ~ 0
RESET
Wire Wire Line
	4800 2700 4350 2700
Text Label 4350 3100 2    50   ~ 0
HSE_IN
Wire Wire Line
	4800 3100 4350 3100
Text Label 4350 3200 2    50   ~ 0
HSE_OUT
Wire Wire Line
	4800 3200 4350 3200
$Comp
L Device:Crystal_GND24_Small Y7101
U 1 1 5FFB4BA1
P 4150 6600
F 0 "Y7101" H 4250 6700 50  0000 L CNN
F 1 "16MHz" H 3800 6700 50  0000 L CNN
F 2 "Oscillator:Oscillator_SMD_SeikoEpson_SG8002CE-4Pin_3.2x2.5mm_HandSoldering" H 4150 6600 50  0001 C CNN
F 3 "~" H 4150 6600 50  0001 C CNN
	1    4150 6600
	1    0    0    -1  
$EndComp
Text Label 3600 7000 2    50   ~ 0
GND
Wire Wire Line
	3600 7000 3850 7000
Text Label 3600 6600 2    50   ~ 0
HSE_IN
Wire Wire Line
	4050 6600 3850 6600
Text Label 4950 6600 0    50   ~ 0
HSE_OUT
$Comp
L Device:R_Small R7106
U 1 1 5FFD67D4
P 4650 6600
F 0 "R7106" V 4454 6600 50  0000 C CNN
F 1 "47" V 4545 6600 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4650 6600 50  0001 C CNN
F 3 "~" H 4650 6600 50  0001 C CNN
	1    4650 6600
	0    1    1    0   
$EndComp
Wire Wire Line
	4750 6600 4950 6600
Wire Wire Line
	4550 6600 4400 6600
$Comp
L Device:C_Small C7102
U 1 1 5FFDFB1B
P 3850 6800
F 0 "C7102" H 3942 6846 50  0000 L CNN
F 1 "12p" H 3942 6755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3850 6800 50  0001 C CNN
F 3 "~" H 3850 6800 50  0001 C CNN
	1    3850 6800
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C7103
U 1 1 5FFE8492
P 4400 6800
F 0 "C7103" H 4492 6846 50  0000 L CNN
F 1 "12p" H 4492 6755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4400 6800 50  0001 C CNN
F 3 "~" H 4400 6800 50  0001 C CNN
	1    4400 6800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 6700 4400 6600
Connection ~ 4400 6600
Wire Wire Line
	4400 6600 4250 6600
Wire Wire Line
	3850 6700 3850 6600
Connection ~ 3850 6600
Wire Wire Line
	3850 6600 3600 6600
Text Label 3600 6400 2    50   ~ 0
GND
Wire Wire Line
	4150 6400 4150 6500
Wire Wire Line
	3600 6400 4150 6400
Wire Wire Line
	3850 6900 3850 7000
Connection ~ 3850 7000
Wire Wire Line
	4400 7000 4400 6900
Wire Wire Line
	4150 6700 4150 7000
Connection ~ 4150 7000
Wire Wire Line
	4150 7000 4400 7000
Wire Wire Line
	3850 7000 4150 7000
Text Notes 3300 7200 0    50   ~ 0
C_L=2*(C_LOAD_CRYSTAL-C_PARASITIC)
Text Notes 3300 7350 0    50   ~ 0
C_L=2*(9p-2p)=12p
Wire Wire Line
	6100 5000 6550 5000
Text Label 6550 5000 0    50   ~ 0
uC_USB_D+
Wire Wire Line
	6100 4900 6550 4900
Text Label 6550 4900 0    50   ~ 0
uC_USB_D-
Wire Wire Line
	4800 4500 4350 4500
Text Label 4350 4500 2    50   ~ 0
PB_7_SDA_1
Wire Wire Line
	4800 4400 4350 4400
Text Label 4350 4400 2    50   ~ 0
PB_6_SCL_1
Text HLabel 1500 6850 0    50   Input ~ 0
USB_D+
Text Label 1950 6850 0    50   ~ 0
USB_D+
Wire Wire Line
	1500 6850 1950 6850
Text HLabel 1500 6950 0    50   Input ~ 0
USB_D-
Text Label 1950 6950 0    50   ~ 0
USB_D-
Wire Wire Line
	1500 6950 1950 6950
Text Label 1350 2300 2    50   ~ 0
PB_2_BOOT_1
Text Label 1350 2050 2    50   ~ 0
BOOT_0
$Comp
L Device:R_Small R7104
U 1 1 600397EC
P 1650 2300
F 0 "R7104" V 1750 2200 50  0000 L CNN
F 1 "10k" V 1700 2050 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 1650 2300 50  0001 C CNN
F 3 "~" H 1650 2300 50  0001 C CNN
	1    1650 2300
	0    -1   -1   0   
$EndComp
Text Label 1950 2300 0    50   ~ 0
GND
Text Label 1950 2050 0    50   ~ 0
GND
Wire Wire Line
	6100 4100 6550 4100
Text Label 6550 4100 0    50   ~ 0
PA_3
Wire Wire Line
	6100 4200 6550 4200
Text Label 6550 4200 0    50   ~ 0
PA_4
Wire Wire Line
	4800 4100 4350 4100
Text Label 4350 4100 2    50   ~ 0
PB_3_SWO
$Comp
L Device:R_Small R7103
U 1 1 60079711
P 1650 2050
F 0 "R7103" V 1750 1950 50  0000 L CNN
F 1 "10k" V 1700 1800 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 1650 2050 50  0001 C CNN
F 3 "~" H 1650 2050 50  0001 C CNN
	1    1650 2050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1350 2050 1450 2050
Wire Wire Line
	1750 2050 1950 2050
Wire Wire Line
	1750 2300 1950 2300
Wire Wire Line
	1350 2300 1450 2300
Wire Wire Line
	4800 5300 4350 5300
Text Label 4350 5300 2    50   ~ 0
PB_15
Wire Wire Line
	4800 5200 4350 5200
Text Label 4350 5200 2    50   ~ 0
PB_14
Wire Wire Line
	4800 3400 4350 3400
Text Label 4350 3400 2    50   ~ 0
PC_13
Wire Wire Line
	4800 3500 4350 3500
Text Label 4350 3500 2    50   ~ 0
PC_14
Wire Wire Line
	4800 3600 4350 3600
Text Label 4350 3600 2    50   ~ 0
PC_15
Wire Wire Line
	4800 3800 4350 3800
Text Label 4350 3800 2    50   ~ 0
PB_0
Wire Wire Line
	4800 3900 4350 3900
Text Label 4350 3900 2    50   ~ 0
PB_1
Wire Wire Line
	4800 4200 4350 4200
Text Label 4350 4200 2    50   ~ 0
PB_4
Wire Wire Line
	4800 4300 4350 4300
Text Label 4350 4300 2    50   ~ 0
PB_5
Wire Wire Line
	4800 4600 4350 4600
Text Label 4350 4600 2    50   ~ 0
PB_8
Wire Wire Line
	4800 4700 4350 4700
Text Label 4350 4700 2    50   ~ 0
PB_9
Wire Wire Line
	4800 4800 4350 4800
Text Label 4350 4800 2    50   ~ 0
PB_10
Wire Wire Line
	4800 4900 4350 4900
Text Label 4350 4900 2    50   ~ 0
PB_11
Wire Wire Line
	4800 5000 4350 5000
Text Label 4350 5000 2    50   ~ 0
PB_12
Wire Wire Line
	4800 5100 4350 5100
Text Label 4350 5100 2    50   ~ 0
PB_13
Wire Wire Line
	6100 3800 6550 3800
Text Label 6550 3800 0    50   ~ 0
PA_0
Wire Wire Line
	6100 3900 6550 3900
Text Label 6550 3900 0    50   ~ 0
PA_1
Wire Wire Line
	6100 4000 6550 4000
Text Label 6550 4000 0    50   ~ 0
PA_2
Wire Wire Line
	6100 4300 6550 4300
Text Label 6550 4300 0    50   ~ 0
PA_5
Wire Wire Line
	6100 4400 6550 4400
Text Label 6550 4400 0    50   ~ 0
PA_6
Wire Wire Line
	6100 4500 6550 4500
Text Label 6550 4500 0    50   ~ 0
PA_7
Wire Wire Line
	6100 4600 6550 4600
Text Label 6550 4600 0    50   ~ 0
PA_8
Wire Wire Line
	6100 4700 6550 4700
Text Label 6550 4700 0    50   ~ 0
PA_9
Wire Wire Line
	6100 4800 6550 4800
Text Label 6550 4800 0    50   ~ 0
PA_10
Wire Wire Line
	6100 5100 6550 5100
Text Label 6550 5100 0    50   ~ 0
PA_13_SWDIO
Wire Wire Line
	6100 5200 6550 5200
Text Label 6550 5200 0    50   ~ 0
PA_14_SWCLK
Wire Wire Line
	6100 5300 6550 5300
Text Label 6550 5300 0    50   ~ 0
PA_15
Text Label 1950 2550 0    50   ~ 0
2.0÷3.6V
Wire Wire Line
	1950 2550 1750 2550
Text Label 1950 1800 0    50   ~ 0
2.0÷3.6V
Wire Wire Line
	1950 1800 1750 1800
$Comp
L Device:R_Small R7102
U 1 1 600203B7
P 1650 1800
F 0 "R7102" V 1750 1700 50  0000 L CNN
F 1 "10k" V 1700 1550 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 1650 1800 50  0001 C CNN
F 3 "~" H 1650 1800 50  0001 C CNN
	1    1650 1800
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R7105
U 1 1 60020969
P 1650 2550
F 0 "R7105" V 1750 2450 50  0000 L CNN
F 1 "10k" V 1700 2300 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 1650 2550 50  0001 C CNN
F 3 "~" H 1650 2550 50  0001 C CNN
	1    1650 2550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1550 1800 1450 1800
Wire Wire Line
	1450 1800 1450 2050
Connection ~ 1450 2050
Wire Wire Line
	1450 2050 1550 2050
Wire Wire Line
	1550 2550 1450 2550
Wire Wire Line
	1450 2550 1450 2300
Connection ~ 1450 2300
Wire Wire Line
	1450 2300 1550 2300
Text HLabel 1500 3300 0    50   Input ~ 0
PA_0
Text Label 1950 3300 0    50   ~ 0
PA_0
Wire Wire Line
	1500 3300 1950 3300
Text HLabel 1500 3400 0    50   Input ~ 0
PA_1
Text Label 1950 3400 0    50   ~ 0
PA_1
Wire Wire Line
	1500 3400 1950 3400
Text HLabel 1500 3500 0    50   Input ~ 0
PA_2
Text Label 1950 3500 0    50   ~ 0
PA_2
Wire Wire Line
	1500 3500 1950 3500
Text HLabel 1500 3600 0    50   Input ~ 0
PA_3
Text Label 1950 3600 0    50   ~ 0
PA_3
Wire Wire Line
	1500 3600 1950 3600
Text HLabel 1500 3700 0    50   Input ~ 0
PA_4
Text Label 1950 3700 0    50   ~ 0
PA_4
Wire Wire Line
	1500 3700 1950 3700
Text HLabel 1500 3800 0    50   Input ~ 0
PA_5
Text Label 1950 3800 0    50   ~ 0
PA_5
Wire Wire Line
	1500 3800 1950 3800
Text HLabel 1500 3900 0    50   Input ~ 0
PA_6
Text Label 1950 3900 0    50   ~ 0
PA_6
Wire Wire Line
	1500 3900 1950 3900
Text HLabel 1500 4000 0    50   Input ~ 0
PA_7
Text Label 1950 4000 0    50   ~ 0
PA_7
Wire Wire Line
	1500 4000 1950 4000
Text HLabel 1500 4100 0    50   Input ~ 0
PA_8
Text Label 1950 4100 0    50   ~ 0
PA_8
Wire Wire Line
	1500 4100 1950 4100
Text HLabel 1500 4200 0    50   Input ~ 0
PA_9
Text Label 1950 4200 0    50   ~ 0
PA_9
Wire Wire Line
	1500 4200 1950 4200
Text HLabel 1500 4300 0    50   Input ~ 0
PA_10
Text Label 1950 4300 0    50   ~ 0
PA_10
Wire Wire Line
	1500 4300 1950 4300
Text HLabel 1500 4400 0    50   Input ~ 0
PA_11
Text Label 1950 4400 0    50   ~ 0
PA_11
Wire Wire Line
	1500 4400 1950 4400
Text HLabel 1500 4500 0    50   Input ~ 0
PA_12
Text Label 1950 4500 0    50   ~ 0
PA_12
Wire Wire Line
	1500 4500 1950 4500
Text HLabel 1500 4600 0    50   Input ~ 0
PA_13_SWDIO
Text Label 1950 4600 0    50   ~ 0
PA_13_SWDIO
Wire Wire Line
	1500 4600 1950 4600
Text HLabel 1500 4700 0    50   Input ~ 0
PA_14_SWCLK
Text Label 1950 4700 0    50   ~ 0
PA_14_SWCLK
Wire Wire Line
	1500 4700 1950 4700
Text HLabel 1500 4800 0    50   Input ~ 0
PA_15
Text Label 1950 4800 0    50   ~ 0
PA_15
Wire Wire Line
	1500 4800 1950 4800
Text HLabel 1500 5050 0    50   Input ~ 0
PB_0
Text Label 1950 5050 0    50   ~ 0
PB_0
Wire Wire Line
	1500 5050 1950 5050
Text HLabel 1500 5150 0    50   Input ~ 0
PB_1
Text Label 1950 5150 0    50   ~ 0
PB_1
Wire Wire Line
	1500 5150 1950 5150
Text HLabel 1500 5250 0    50   Input ~ 0
PB_2_BOOT_1
Text Label 1950 5250 0    50   ~ 0
PB_2_BOOT_1
Wire Wire Line
	1500 5250 1950 5250
Text HLabel 1500 5350 0    50   Input ~ 0
PB_3_SWO
Text Label 1950 5350 0    50   ~ 0
PB_3_SWO
Wire Wire Line
	1500 5350 1950 5350
Text HLabel 1500 5450 0    50   Input ~ 0
PB_4
Text Label 1950 5450 0    50   ~ 0
PB_4
Wire Wire Line
	1500 5450 1950 5450
Text HLabel 1500 5550 0    50   Input ~ 0
PB_5
Text Label 1950 5550 0    50   ~ 0
PB_5
Wire Wire Line
	1500 5550 1950 5550
Text HLabel 1500 5650 0    50   Input ~ 0
PB_6_SCL_1
Text Label 1950 5650 0    50   ~ 0
PB_6_SCL_1
Wire Wire Line
	1500 5650 1950 5650
Text HLabel 1500 5750 0    50   Input ~ 0
PB_7_SDA_1
Text Label 1950 5750 0    50   ~ 0
PB_7_SDA_1
Wire Wire Line
	1500 5750 1950 5750
Text HLabel 1500 5850 0    50   Input ~ 0
PB_8
Text Label 1950 5850 0    50   ~ 0
PB_8
Wire Wire Line
	1500 5850 1950 5850
Text Label 1950 5950 0    50   ~ 0
PB_9
Wire Wire Line
	1500 5950 1950 5950
Text HLabel 1500 6050 0    50   Input ~ 0
PB_10
Text Label 1950 6050 0    50   ~ 0
PB_10
Wire Wire Line
	1500 6050 1950 6050
Text HLabel 1500 6150 0    50   Input ~ 0
PB_11
Text Label 1950 6150 0    50   ~ 0
PB_11
Wire Wire Line
	1500 6150 1950 6150
Text HLabel 1500 6250 0    50   Input ~ 0
PB_12
Text Label 1950 6250 0    50   ~ 0
PB_12
Wire Wire Line
	1500 6250 1950 6250
Text HLabel 1500 6350 0    50   Input ~ 0
PB_13
Text Label 1950 6350 0    50   ~ 0
PB_13
Wire Wire Line
	1500 6350 1950 6350
Text HLabel 1500 6450 0    50   Input ~ 0
PB_14
Text Label 1950 6450 0    50   ~ 0
PB_14
Wire Wire Line
	1500 6450 1950 6450
Text HLabel 1500 6550 0    50   Input ~ 0
PB_15
Text Label 1950 6550 0    50   ~ 0
PB_15
Wire Wire Line
	1500 6550 1950 6550
Text HLabel 1500 5950 0    50   Input ~ 0
PB_9
$Comp
L Device:L_Small L7101
U 1 1 600F01AE
P 6750 2350
F 0 "L7101" V 6935 2350 50  0000 C CNN
F 1 "39n" V 6844 2350 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric" H 6750 2350 50  0001 C CNN
F 3 "~" H 6750 2350 50  0001 C CNN
	1    6750 2350
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x06 J7102
U 1 1 601B6497
P 9800 2100
F 0 "J7102" H 9880 2092 50  0000 L CNN
F 1 "Conn_01x06" H 9880 2001 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 9800 2100 50  0001 C CNN
F 3 "~" H 9800 2100 50  0001 C CNN
	1    9800 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9600 2200 9350 2200
Text Label 9350 2200 2    50   ~ 0
PA_13_SWDIO
Wire Wire Line
	9600 2000 9350 2000
Text Label 9350 2000 2    50   ~ 0
PA_14_SWCLK
Text Label 9350 2300 2    50   ~ 0
RESET
Wire Wire Line
	9600 2300 9350 2300
Wire Wire Line
	9600 2400 9350 2400
Text Label 9350 2400 2    50   ~ 0
PB_3_SWO
Text Label 9350 2100 2    50   ~ 0
GND
Wire Wire Line
	9600 2100 9350 2100
Text Label 8850 3950 2    50   ~ 0
PB_6_SCL_1
Text Label 8850 3650 2    50   ~ 0
PB_7_SDA_1
Text Label 9800 3650 0    50   ~ 0
2.0÷3.6V
Wire Wire Line
	9800 3650 9600 3650
$Comp
L Device:R_Small R7109
U 1 1 6027B622
P 9500 3650
F 0 "R7109" V 9600 3600 50  0000 L CNN
F 1 "4.7k" V 9400 3550 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 9500 3650 50  0001 C CNN
F 3 "~" H 9500 3650 50  0001 C CNN
	1    9500 3650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8850 3650 9400 3650
Text Label 9800 3950 0    50   ~ 0
2.0÷3.6V
Wire Wire Line
	9800 3950 9600 3950
$Comp
L Device:R_Small R7110
U 1 1 6029E286
P 9500 3950
F 0 "R7110" V 9600 3900 50  0000 L CNN
F 1 "4.7k" V 9400 3850 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 9500 3950 50  0001 C CNN
F 3 "~" H 9500 3950 50  0001 C CNN
	1    9500 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8850 3950 9400 3950
Text HLabel 1500 7050 0    50   Input ~ 0
V_BUS
Text Label 1950 7050 0    50   ~ 0
V_USB
Wire Wire Line
	1500 7050 1950 7050
Text Label 8450 5600 3    50   ~ 0
V_USB
Wire Wire Line
	8450 5450 8450 5600
$Comp
L Device:R_Small R7111
U 1 1 60016187
P 10100 5200
F 0 "R7111" V 10200 5150 50  0000 L CNN
F 1 "22" V 10000 5150 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 10100 5200 50  0001 C CNN
F 3 "~" H 10100 5200 50  0001 C CNN
	1    10100 5200
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R7108
U 1 1 60016AB2
P 8800 5200
F 0 "R7108" V 8900 5150 50  0000 L CNN
F 1 "22" V 8700 5150 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 8800 5200 50  0001 C CNN
F 3 "~" H 8800 5200 50  0001 C CNN
	1    8800 5200
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small R7107
U 1 1 6002294A
P 8450 5350
F 0 "R7107" V 8550 5300 50  0000 L CNN
F 1 "4.7k" V 8350 5250 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 8450 5350 50  0001 C CNN
F 3 "~" H 8450 5350 50  0001 C CNN
	1    8450 5350
	-1   0    0    1   
$EndComp
$Comp
L Power_Protection:USBLC6-2SC6 U7102
U 1 1 60046C8E
P 9450 5100
F 0 "U7102" H 9800 5600 50  0000 C CNN
F 1 "USBLC6-2SC6" H 9800 5500 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6" H 9450 4600 50  0001 C CNN
F 3 "https://www.st.com/resource/en/datasheet/usblc6-2.pdf" H 9650 5450 50  0001 C CNN
	1    9450 5100
	-1   0    0    1   
$EndComp
Text Label 8250 5200 2    50   ~ 0
uC_USB_D+
Text Label 10350 5200 0    50   ~ 0
uC_USB_D-
Text Label 9450 4600 1    50   ~ 0
GND
Wire Wire Line
	9450 4700 9450 4600
Text Label 8850 5000 2    50   ~ 0
USB_D+
Text Label 10000 5000 0    50   ~ 0
USB_D-
Wire Wire Line
	10350 5200 10200 5200
Wire Wire Line
	8250 5200 8450 5200
Wire Wire Line
	8450 5250 8450 5200
Connection ~ 8450 5200
Wire Wire Line
	8450 5200 8700 5200
Text Label 9450 5600 3    50   ~ 0
V_USB
Wire Wire Line
	9450 5500 9450 5600
Wire Wire Line
	8850 5000 9000 5000
Wire Wire Line
	10000 5000 9900 5000
Wire Wire Line
	10000 5200 9900 5200
Wire Wire Line
	9050 5200 9000 5200
Wire Wire Line
	9000 5000 9000 5200
Connection ~ 9000 5000
Wire Wire Line
	9000 5000 9050 5000
Connection ~ 9000 5200
Wire Wire Line
	9000 5200 8900 5200
Wire Wire Line
	9900 5000 9900 5200
Connection ~ 9900 5000
Wire Wire Line
	9900 5000 9850 5000
Connection ~ 9900 5200
Wire Wire Line
	9900 5200 9850 5200
Wire Wire Line
	1500 3050 1950 3050
Text Label 1950 3050 0    50   ~ 0
PC_13
Wire Wire Line
	1500 2950 1950 2950
Text Label 1950 2950 0    50   ~ 0
PC_14
Wire Wire Line
	1500 2850 1950 2850
Text Label 1950 2850 0    50   ~ 0
PC_15
Text HLabel 1500 2850 0    50   Input ~ 0
PC_15
Text HLabel 1500 2950 0    50   Input ~ 0
PC_14
Text HLabel 1500 3050 0    50   Input ~ 0
PC_13
NoConn ~ 9600 1900
Text Label 1950 1450 0    50   ~ 0
2.0÷3.6V
Wire Wire Line
	1950 1450 1750 1450
$Comp
L Device:R_Small R7101
U 1 1 608178A2
P 1650 1450
F 0 "R7101" V 1750 1350 50  0000 L CNN
F 1 "10k" V 1700 1200 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 1650 1450 50  0001 C CNN
F 3 "~" H 1650 1450 50  0001 C CNN
	1    1650 1450
	0    -1   -1   0   
$EndComp
Text Label 1350 1450 2    50   ~ 0
RESET
Wire Wire Line
	1350 1450 1500 1450
$Comp
L Switch:SW_Push SW7101
U 1 1 6088B498
P 1850 900
F 0 "SW7101" H 1850 1185 50  0000 C CNN
F 1 "SW_Push" H 1850 1094 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_Push_1P1T_NO_CK_KMR2" H 1850 1100 50  0001 C CNN
F 3 "~" H 1850 1100 50  0001 C CNN
	1    1850 900 
	1    0    0    -1  
$EndComp
Text Label 2250 900  0    50   ~ 0
GND
Wire Wire Line
	2050 900  2150 900 
Wire Wire Line
	1650 900  1500 900 
Wire Wire Line
	1500 900  1500 1050
Connection ~ 1500 1450
Wire Wire Line
	1500 1450 1550 1450
$Comp
L Device:C_Small C7101
U 1 1 608E82FE
P 1850 1050
F 0 "C7101" H 1942 1096 50  0000 L CNN
F 1 "1u" H 1942 1005 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1850 1050 50  0001 C CNN
F 3 "~" H 1850 1050 50  0001 C CNN
	1    1850 1050
	0    1    1    0   
$EndComp
Wire Wire Line
	1950 1050 2150 1050
Wire Wire Line
	2150 1050 2150 900 
Connection ~ 2150 900 
Wire Wire Line
	2150 900  2250 900 
Wire Wire Line
	1750 1050 1500 1050
Connection ~ 1500 1050
Wire Wire Line
	1500 1050 1500 1450
$EndSCHEMATC
